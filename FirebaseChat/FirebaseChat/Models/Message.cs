﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirebaseChat.Models
{
    public class Message
    {
        public string Key { get; set; }
        public string Author { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
    }
}
