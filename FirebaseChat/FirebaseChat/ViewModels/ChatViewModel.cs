﻿using FirebaseChat.Models;
using FirebaseChat.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FirebaseChat.ViewModels
{
    public class ChatViewModel : ViewModelBase
    {
        private ObservableCollection<Message> _Messages;
        public ObservableCollection<Message> Messages
        {
            get { return _Messages = _Messages ?? _MessageStore.GetAll(); }
        }

        private string _Content;

        public string Content
        {
            get { return _Content; }
            set { SetValue(ref _Content, value); }
        }

        private Command _AddCmd;
        public Command AddCmd
        {
            get { return _AddCmd = _AddCmd ?? new Command(Add); }
        }

        private Command _DeleteCmd;
        public Command DeleteCmd
        {
            get { return _DeleteCmd = _DeleteCmd ?? new Command<string>(Delete); }
        }


        private readonly IMessageStore _MessageStore;


        public ChatViewModel()
        {
            _MessageStore = DependencyService.Get<IMessageStore>();
        }


        private void Add()
        {
            _MessageStore.Add(new Message {
                Author = "Khun",
                Content = Content,
                Date = DateTime.Now
            });
            Content = null;
        }

        private void Delete(string key)
        {
            _MessageStore.Delete(key);
        }
    }
}
