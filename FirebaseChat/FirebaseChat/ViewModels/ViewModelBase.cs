﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace FirebaseChat.ViewModels
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }

        protected virtual bool SetValue<T>(ref T oldValue, T newValue, [CallerMemberName] string propName = "")
        {
            if(EqualityComparer<T>.Default.Equals(oldValue, newValue))
            {
                return false;
            }
            oldValue = newValue;
            OnPropertyChanged(propName);
            return true;
        }

    }
}
