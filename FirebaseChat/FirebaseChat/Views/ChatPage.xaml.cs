﻿using FirebaseChat.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FirebaseChat.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatPage : ContentPage
    {
        public ChatPage()
        {
            InitializeComponent();

            (LvMessages.ItemsSource as INotifyCollectionChanged).CollectionChanged
                += (sender, args) => {
                    if(args.NewItems != null)
                    {
                        var item = args.NewItems[args.NewItems.Count - 1];
                        LvMessages.SelectedItem = item;
                    }
                };
        }
    }
}