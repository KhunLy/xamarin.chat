﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using FirebaseChat.Models;

namespace FirebaseChat.Services
{
    interface IMessageStore
    {
        ObservableCollection<Message> GetAll();

        void Add(Message message);

        void Delete(string key);

        void Update(string key, Message message);
    }
}