﻿using Firebase.Database;

namespace FirebaseChat.Services
{
    interface IFBDbProvider
    {
        FirebaseClient Client { get; }
    }
}