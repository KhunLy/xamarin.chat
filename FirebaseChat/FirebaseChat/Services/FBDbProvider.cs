﻿using Firebase.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace FirebaseChat.Services
{
    class FBDbProvider : IFBDbProvider
    {
        private FirebaseClient _Client;

        public FirebaseClient Client
        {
            get
            {
                return _Client = _Client
                    ?? new FirebaseClient("https://xamarinchat-5246b.firebaseio.com/");
            }
        }
    }
}
