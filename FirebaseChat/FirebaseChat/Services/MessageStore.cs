﻿using Firebase.Database;
using Firebase.Database.Streaming;
using FirebaseChat.Models;
using FirebaseChat.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FirebaseChat.Services
{
    class MessageStore : IMessageStore
    {
        private readonly IFBDbProvider _Provider;

        private readonly ObservableCollection<Message> _Messages = new ObservableCollection<Message>();

        public MessageStore()
        {
            _Provider = DependencyService.Get<IFBDbProvider>();
            RefreshContext();
            _Provider.Client.Child("Messages")
                .AsObservable<Message>()
                .Subscribe(RefreshContext);
        }

        private async void RefreshContext(FirebaseEvent<Message> obj = null)
        {
            if (obj == null)
            {
                var items = await _Provider.Client.Child("Messages").OnceAsync<Message>();
                Device.BeginInvokeOnMainThread(() =>
                {
                    _Messages.Clear();
                    foreach (var item in items)
                    {
                        var m = new Message
                        {
                            Key = item.Key,
                            Author = item.Object.Author,
                            Content = item.Object.Content,
                            Date = item.Object.Date
                        };
                        _Messages.Add(m);
                    }
                });
            }
            else
            {
                if(obj.EventType == FirebaseEventType.Delete)
                {
                    Message toDelete = _Messages.FirstOrDefault(m => m.Key == obj.Key);
                    Device.BeginInvokeOnMainThread(() => {
                        _Messages.Remove(toDelete);
                    });
                }
                else
                {
                    Message toUpdate = _Messages.FirstOrDefault(m => m.Key == obj.Key);
                    if (toUpdate == null)
                    {
                        Device.BeginInvokeOnMainThread(() => {
                            _Messages.Add(new Message
                            {
                                Key = obj.Key,
                                Author = obj.Object.Author,
                                Content = obj.Object.Content,
                                Date = obj.Object.Date
                            });
                        });
                        
                    }
                    else
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            toUpdate.Content = obj.Object.Content;
                            toUpdate.Date = obj.Object.Date;
                            toUpdate.Author = obj.Object.Author;
                        });
                    }
                }
            }
        }

        public void Add(Message message)
        {
            _Provider.Client.Child("Messages")
                .PostAsync(JsonConvert.SerializeObject(message));
        }

        public void Delete(string key)
        {
            _Provider.Client.Child("Messages/" + key)
                .DeleteAsync();
        }

        public ObservableCollection<Message> GetAll()
        {
            return _Messages;
        }

        public void Update(string key, Message message)
        {
            _Provider.Client.Child("Messages/" + key)
                .PutAsync(JsonConvert.SerializeObject(message));
        }
    }
}
